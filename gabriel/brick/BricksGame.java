
import java.awt.*;
import java.util.Vector;
import java.util.Date;
import java.util.Random;
import java.lang.Math;


public class BricksGame extends Canvas {
  private BricksApplet parent;
  private Color brickColor;
  Image osI;
  Graphics osG;
  int rows, cols, level=0;
  int radius;

  Brick bricks[][];
  Ball ball;
  Paddle paddle;
  Wall wall;

  public BricksGame (BricksApplet parent) {
    System.out.println("Bricks:Creating");

    this.parent = parent;

    level = 0;
    rows = 4; cols = 6;
    brickColor = Color.red;
  }

  public void nextLevel() {

    level++;
    System.out.println(this + "Next Level (" + level + ").");

    switch (level) {
      case 1:
        rows = 4; cols = 8;
        brickColor = Color.blue;
        break;
      case 2:
        rows = 5; cols = 10;
        brickColor = Color.green;
        break;
      case 3:
        rows = 6; cols = 12;
        brickColor = Color.magenta;
        break;
      case 4:
        rows = 7; cols = 14;
        brickColor = Color.white;
        break;
      default:
        rows = 3 + level; cols = 6 + 2 * level;
        brickColor = Color.magenta;
      }

      System.out.println(this + "Level " + level + ".");

      osI = null;
      osG = null;

      repaint();

  }

  public void pause () {
    System.out.println(this + "Pause");
    if (ball != null) ball.suspend();
  }

  public void resume () {
    System.out.println(this + "resume");
    if (ball != null) ball.resume();
  }

  public void stop () {
    System.out.println(this + "Stop");
    if (ball != null) ball.stop();
  }

  public void destroy () {
    System.out.println(this + "Destroy");
    if (ball != null) ball.stop();
  }

  public void start () {
    System.out.println(this + "Start");
    if (ball != null) ball.start();
  }

  public void reset () {
    System.out.println(this + "Reset");

    rows = 4; cols = 6;
    brickColor = Color.red;

    if (ball != null) ball.stop();

    resize();
    repaint();
  }

  public void play(int sound) {
    parent.getEfx()[sound].play();
  }

  public void resize () {
    System.out.println("Bricks:Setup.");

    osI = this.createImage(this.size().width, this.size().height);
    osG = osI.getGraphics();
    this.setBackground(Color.black);
    this.setForeground(Color.white);

    System.out.println("Bricks:Setup Objects (" +
		this.size().width + "," + this.size().height + ").");

    wall = new Wall(1, 1, this.size().width - 1, this.size().height - 1, Color.green, osG);

    bricks = new Brick[rows][cols];
    int dw, dh, top;
    dw = (int) (wall.width / cols);
    dh = (int) (wall.height * 0.4f / rows);
    top = dh;

    System.out.println("Bricks: dw=" + dw + ", dh=" + dh + ", top=" + top + ".");
    
    int x, y;

    for (y=0; y < rows; ++y) {
      if (y > 0) brickColor = brickColor.darker();

      for (x=0; x < cols; ++x) 
        bricks[y][x] = new Brick(
		wall.left + (int) (dw * x + 0.025f*dw), 
		wall.top + (int) (top + y * dh + 0.05f*dh),
		wall.left + (int) (dw * (x+1) - 0.05f*dw), 
		wall.top + (int) (top + dh * (y+1) - 0.05f*dh),
                brickColor,
                rows - y + 1);
    }

    bricks[0][0].setGraphics(osG);

    Random rand = new Random();

    paddle = new Paddle(
        (int) (wall.width * 0.8f * rand.nextFloat() + wall.width * 0.1f),
	(int) (wall.height * 0.95f),
	(int) (wall.width * 0.1f),
	(int) (wall.height * 0.02f),
	Color.red, osG);

    // clear the offscreen
    osG.setColor(this.getBackground());
    osG.fillRect(0, 0, this.size().width, this.size().height);
    osG.setColor(this.getForeground());

    // this is actually the first time here
    redraw();

    ball = new Ball(
	wall.left + (int) (wall.width * 0.8f * rand.nextFloat() + wall.width * 0.05f),
        wall.top + (int) (wall.height * 0.2f * rand.nextFloat() + top + rows * dh + radius + 2),
        (int) (wall.width * 0.03f), 
	level, 
        ball != null ? ball.getScore() : 0,
	Color.white);
    ball.init(this, osG);

    // start the ball in motion
    ball.start();

  }

  public void redraw () {
  // what to do after an impact

    // draw the bricks
    int i, j;
    for (i=0; i < cols; ++i) 
      for (j=0; j < rows; ++j) 
        bricks[j][i].draw();

    // draw the wall
    wall.draw();
  }
    

  public void update (Graphics g) {
    paint(g);
  }

  public void paint (Graphics g) {

    if (osI == null) 
      this.resize();

    // draw the paddle
    paddle.draw();

    // copy the offscreen to the real screen
    g.drawImage(osI, 0, 0, this);
  }

  public boolean mouseDown(Event e, int x, int y) {
    System.out.println(this + "Coordinate (" + x + "," + y + ").");
    return true;
  }

  public boolean mouseDrag (Event e, int x, int y) {

    paddle.undraw();

    if (x > this.size().width) {
      x = this.size().width - paddle.width - 2;
      wall.draw();
    } else if (x < -1 * paddle.width) {
      x = 2;
      wall.draw();
    }

    paddle.moveX(x);
//    ball.undraw();
//    ball.move(x, y);

    repaint();
    return true;
  }

  public boolean mouseUp (Event e, int x, int y) {
/*    System.out.println("Lines:New Line.");

    if (e.shiftDown()) {
      if (java.lang.Math.abs(end.x - start.x) > java.lang.Math.abs(end.y - start.y)) 
        y=start.y;
      else
        x=start.x;
    }
    end.move(x, y);

    tmp = new Rect(start, end);
    lines.addElement(tmp);

    parent.points.addItem("(" + tmp.x1 + "," + tmp.y1 + ") :  (" + tmp.x2 + "," + tmp.y2 + ")");
    parent.points.makeVisible(parent.points.countItems()-1);

    start = null; end = null;
*/
    repaint();

    return true;
  }

  public final Ball getBall() {
    return ball;
  }

  public final Wall getWall() {
    return wall;
  }

  public String toString () {
    return "BricksGame::";
  }

}

