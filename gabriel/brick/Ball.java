import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;
import java.applet.AudioClip;

public class Ball extends Thread {
  private BricksGame parent;
  private Graphics g;
  private int x, y, xl, yl;
  private int initial_x, initial_y;
  private int radius, score;
  private int speed, sleep;
  private Color color;
  private int direction;
  private Point tmp;
  private Random rand;

  /** 
	0 degrees is right horizontal
  */ 
  final static int DIR_HORIZ = 0;
  final static int DIR_MAX = 180;
  final static int DIR_VERT = DIR_MAX / 2;

  public Ball (int x, int y, int radius, int level, int score, Color color) {
    rand = new Random();

    this.score = score;

    initial_x = x;
    initial_y = y;

    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    direction = -1 * (DIR_VERT / 2 + (int) ((DIR_VERT * rand.nextFloat()) / 8));

    tmp = new Point(0,0);

    switch (level) {
      case 0:
        speed = 6;
        sleep = 12;
        break;
      case 1:
        speed = 5;
        sleep = 10;
        break;
      case 2:
        speed = 5;
        sleep = 8;
        break;
      case 3:
        speed = 6;
        sleep = 12;
        break;
      case 4:
        speed = 6;
        sleep = 10;
        break;
      default:
        speed = 1 + level;
        sleep = 8;
        break;
    }

    System.out.println("Ball:New Ball! loc: (" + x + "," + y + "), @ (" + speed + ":" + sleep + "," + direction + ").");
  }

  public void init (BricksGame parent, Graphics g) {
    this.parent = parent;
    this.g = g;
  }

  public int speedx() {
    return (int) (java.lang.Math.cos(direction * java.lang.Math.PI / DIR_MAX) * speed);
  }

  public int speedy() {
    return (int) (java.lang.Math.sin(direction * java.lang.Math.PI / DIR_MAX) * speed);
  }

  public void run () {
    boolean tmp, impact=false;
    int remain=0;

    xl = x; yl = y;
    draw();
    parent.repaint();

    try {
      sleep(3600 * 1);
    } catch (InterruptedException e) {
      System.out.println("Ball:: " + e);
    }

    this.setPriority(9);

    while (true) {

      xl = x; yl = y;
      x += speedx(); y += speedy();

      impact = parent.paddle.impact(this);
      if (impact) addScore(1);

      for (int i=0; i < parent.bricks.length; ++i)
        for (int j=0; j < parent.bricks[0].length; ++j) {
          if (parent.bricks[i][j].active) ++remain;
          if (!impact) {
            impact = parent.bricks[i][j].impact(this);
          }
        }


      if (!impact) {
        impact = parent.wall.impact(this);
      }
        
      draw();
      if (impact) parent.wall.draw();

      parent.repaint();

      if (parent.wall.outofBounds(this)) {
        xl = x; yl = y;
        x = initial_x; y=initial_y;

        score = 0;

        draw();
        parent.repaint();

        play(BricksConstants.EFX_LOST_BALL);

        try {
          sleep(3600 * 1);
        } catch (InterruptedException e) {
          System.out.println("Ball:: " + e);
        }

      }

      if (remain == 0) {

        parent.nextLevel();
        play(BricksConstants.EFX_NEXT_LEVEL);

        System.out.println(this + "Killing myself.");

        this.stop();
      }

      impact=false;
      remain=0;
      
      if (sleep > 0) {
        try {
          sleep(sleep);
        } catch (InterruptedException e) {
          System.out.println("Ball:: " + e);
        }
      }

    }

  }

  public void impact (int approach, int a, int b, float slick) {
//    System.out.println(toString() + " IMPACT! ");

    int dd, da;

    if (a > b) System.out.println("Ball: impact vector out of order.");

    if (approach == BricksConstants.IMPACT_LEFT || 
	approach == BricksConstants.IMPACT_RIGHT) {
      if (a > y || b < y) System.out.println("Ball: Invalid impact vector");
     
      if (a == y) a -= 1;
      if (b == y) b += 1;

      if ((dd = (int) (a + (b - a) * 0.4f)) > y) {
        da = (int) ( (float) (y - dd) / (b - a) * DIR_MAX * slick);

/*
        System.out.println(this + "LEFT V; dd=" + dd + "; da=" + da + "; slick=" + slick + 
			"; a,b=" + a + "," + b + "; y=" + y + ".");
*/

        direction += sign(direction) * (DIR_VERT - java.lang.Math.abs(direction)) * 2 + da;

      } else if ((dd = (int) (b - (b - a) * 0.4f)) < y) {

        da = (int) ( (float) (y - dd) / (b - a) * DIR_MAX * slick);
/*
        System.out.println(this + "RIGHT V; dd=" + dd + "; slick=" + slick + 
			"; a,b=" + a + "," + b + "; y=" + y + ".");
*/

        direction += sign(direction) * 
		(DIR_VERT - java.lang.Math.abs(direction)) * 2 + da;

      } else {
        direction += sign(direction) * 
		(DIR_VERT - java.lang.Math.abs(direction)) * 2;
      }

    } else
 
    if (approach == BricksConstants.IMPACT_TOP ||
	approach == BricksConstants.IMPACT_BOTTOM) {
      if (a > x || b < x) System.out.println("Ball: Invalid impact vector");
     
      if (a == x) a -= 1;
      if (b == x) b += 1;

      if ((dd = (int) (a + (b - a) * 0.4f)) > x) {
        da = (int) ( (float) (x - dd) / (b - a) * DIR_MAX * slick);

/*
        System.out.println(this + "LEFT H; dd=" + dd + "; da=" + da + "; slick=" + slick + 
			"; a,b=" + a + "," + b + "; x=" + x + ".");
*/

        direction = -1 * direction + da;

      } else if ((dd = (int) (b - (b - a) * 0.4f)) < x) {

        da = (int) ((float) (x - dd) / (b - a) * DIR_MAX * slick);

/*
        System.out.println(this + "RIGHT H; dd=" + dd + "; da=" + da + "; slick=" + slick + 
			"; a,b=" + a + "," + b + "; x=" + x + ".");
*/

        direction = -1 * direction + da;

      } else
        direction = -1 * direction;

    } else 
      return;

    if (java.lang.Math.abs(speedy()) < 1) {
      System.out.println(this + "Getting out of horizontal stalness.");
      direction += sign(speedy()) * 
        java.lang.Math.abs((int) (rand.nextGaussian() * DIR_MAX * 0.1d));
    } else {
      direction += (int) (rand.nextGaussian() * DIR_MAX * 0.01d);
    }
      
    direction = direction % DIR_MAX;
  }

  public void play (int snd) {
    parent.play(snd);
  }

  public int sign (int x) {
    if (x == 0) return 1;
    return (x / java.lang.Math.abs(x));
  }

  public void draw () {
    synchronized(g) {
      g.setColor(Color.black);
      g.fillOval(xl, yl, radius, radius);

      g.setColor(color);
      g.fillOval(x, y, radius, radius);
    }
  }

  public int setScore(int val) {
    score=val;
    return score;
  }

  public int getScore() {
    return score;
  }

  public int addScore(int val) {
    score += val;

    System.out.println(this + " Score = " + score + ".");

    return score;
  }

  public void setSpeed (int speed) {
    this.speed = speed;
  }

  public Point left() {
    tmp.x = x;
    tmp.y = y + radius / 2;
    return tmp;
  }

  public Point right() {
    tmp.x = x + radius;
    tmp.y = y + radius / 2;
    return tmp;
  }

  public Point top() {
    tmp.x = x + radius / 2;
    tmp.y = y;
    return tmp;
  }

  public Point bottom() {
    tmp.x = x + radius / 2;
    tmp.y = y + radius;
    return tmp;
  }

  public String toString() {
    return ("Ball:: (" + x + "," + y + "); (" + speed + "," + direction + ").");
  }

}

