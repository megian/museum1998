
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Color;

public class Wall {
  public int left, right, top, bottom;
  public int width, height;
  private Color color;
  private Graphics g;

  public Wall (int x1, int y1, int x2, int y2, Color color, Graphics g) {
    System.out.println("Wall:: New Wall!");

    left = x1; right = x2; top = y1; bottom = y2;

    width = right - left;
    height = bottom - top;

    this.g = g;
    this.color = color;
  }

  public boolean outofBounds(Ball b) {
    if (b.bottom().y > bottom + 2) {
      b.play(BricksConstants.EFX_LOST_BALL);
      return true;
    } else {
      return false;
    }
  }

  public boolean impact (Ball b) {

    if (b.left().x + b.speedx() - 1 < left) {

      b.play(BricksConstants.EFX_IMPACT_WALL);
      b.impact(BricksConstants.IMPACT_LEFT, top, bottom, 0.05f);

    } else if (b.right().x + b.speedx() + 1 > right) {

      b.play(BricksConstants.EFX_IMPACT_WALL);
      b.impact(BricksConstants.IMPACT_RIGHT, top, bottom, 0.05f);

    } else if (b.top().y + b.speedy() - 1 < top) {

      b.play(BricksConstants.EFX_IMPACT_WALL);
      b.impact(BricksConstants.IMPACT_TOP, left, right, 0.05f);

/*
    } else if (b.bottom().y + b.speedy() + 1 > bottom) {

      b.play(BricksConstants.EFX_IMPACT_WALL);
      b.impact(BricksConstants.IMPACT_BOTTOM, left, right, 0.05f);
*/

    } else {
      // no impact
      return false;
    }

    return true;

  }

  public void draw () {
    synchronized(g) {
      g.setColor(color);
      g.drawLine(left, top, left, height);
      g.drawLine(width, top, width, height);
      g.drawLine(left, top, width, top);
//      g.drawRect(left, top, width, height);
    }
  }

  public String toString () {
    return "Wall::";
  }

}

