
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

public class Brick {
  public int x1, x2, y1, y2;
  public boolean active;
  int value, rnd;
  Color color;
  static Graphics g;
  private Point tmp;

  public Brick (int x1, int y1, int x2, int y2, Color color, int value) {
//    System.out.println("Brick:: New Brick!" + color);

    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    this.value = value;
    this.color = color;

    active=true;

//    System.out.println(toString());
  }

  public void setGraphics (Graphics g) {
    this.g = g;
  }

  public void draw () {
    if (active) {
      rnd = (int) ((y2 - y1) * 0.5f);
      synchronized(g) {
        g.setColor(color);
        g.fillRoundRect(x1, y1, x2 - x1, y2 - y1, rnd, rnd);
      }
    }
  }

  public void undraw () {
    synchronized(g) {
      g.setColor(Color.black);
      g.fillRect(x1, y1, x2 - x1, y2 - y1);
    }
  }

  public void setActive(boolean state) {
    this.active = state;
  }

  public void setColor(Color color) {
    this.color = color;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int val) {
    this.value = val;
  }

  public boolean impact (Ball b) {
    if (!active) return false;

    if 
    ((tmp = b.right()).x >= x1 && tmp.x <= x2 && tmp.y >= y1 && tmp.y <= y2) {
      System.out.println(this + "Right Impact.");
      b.play(BricksConstants.EFX_IMPACT_BRICK);
      b.impact(BricksConstants.IMPACT_RIGHT, y1, y2, 0.1f);
    } else if 

    ((tmp = b.left()).x >= x1 && tmp.x <= x2 && tmp.y >= y1 && tmp.y <= y2) {
      System.out.println(this + "Left Impact.");
      b.play(BricksConstants.EFX_IMPACT_BRICK);
      b.impact(BricksConstants.IMPACT_LEFT, y1, y2, 0.1f);
    } else if 

    ((tmp = b.top()).x >= x1 && tmp.x <= x2 && tmp.y >= y1 && tmp.y <= y2) {
      System.out.println(this + "Top Impact.");
      b.play(BricksConstants.EFX_IMPACT_BRICK);
      b.impact(BricksConstants.IMPACT_TOP, x1, x2, 0.1f);
    } else if 

    ((tmp = b.bottom()).x >= x1 && tmp.x <= x2 && tmp.y >= y1 && tmp.y <= y2) {
      System.out.println(this + "Bottom Impact.");
      b.play(BricksConstants.EFX_IMPACT_BRICK);
      b.impact(BricksConstants.IMPACT_BOTTOM, x1, x2, 0.1f);
    } else {
      // no impact here
      return false;
    }

    // impact
    active = false;
    undraw();
    b.addScore(value);

    return true;
  }

  public String toString () {
    return ("Brick:: (" + x1 + "," + y1 + ").");
  }
}

