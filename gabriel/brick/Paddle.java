
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;

public class Paddle {
  int x, y, width, height;
  private int max_speed;
  private Color color;
  private Point tmp;
  private Graphics g;

  public Paddle (int x, int y, int width, int height, Color color, Graphics g) {
    System.out.println("Paddle:: New Paddle!");

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.g = g;

    max_speed = Integer.MAX_VALUE;

    System.out.println(toString());
  }

  public boolean impact (Ball b) {
//    System.out.println(this + "Checking for impact.");

    if 
    ((tmp = b.bottom()).x + b.speedx() > x - 2 && tmp.x + b.speedx() < x + width + 2 && 
	tmp.y + b.speedy() > y && tmp.y + b.speedy() <= y + height) {

      b.play(BricksConstants.EFX_IMPACT_PADDLE);
      b.impact(BricksConstants.IMPACT_BOTTOM, x, x + width, 0.5f);

/*
    } else if 
    ((tmp = b.top()).x >= x && tmp.x <= x + width && tmp.y >= y && tmp.y <= y + height) {
      b.play(BricksConstants.EFX_IMPACT_PADDLE);
      b.impact(BricksConstants.IMPACT_TOP, x, x + width, 1f);

    } else if
    ((tmp = b.right()).x >= x && tmp.x <= x + width && tmp.y >= y && tmp.y <= y + height) {
      b.play(BricksConstants.EFX_IMPACT_PADDLE);
      b.impact(BricksConstants.IMPACT_RIGHT, y, y + height, 0.0f);

    } else if 
    ((tmp = b.left()).x >= x && tmp.x <= x + width && tmp.y >= y && tmp.y <= y + height) {
      b.play(BricksConstants.EFX_IMPACT_PADDLE);
      b.impact(BricksConstants.IMPACT_LEFT, y, y + height, 0.0f);

*/

    } else {
//    System.out.println(this + "no impact.");
      return false;
    }

    return true;
  }

  public void setMaxSpeed(int max) {
    max_speed = max;
  }

  public void moveX(int newx) {
    if (java.lang.Math.abs(newx - x) > max_speed) 
      x = newx / java.lang.Math.abs(newx) * max_speed;
    else 
      x = newx;
  }

  public void draw () {
    synchronized(g) {
      g.setColor(color);
      g.fillRect(x, y, width, height);
    }
  }

  public void undraw () {
    synchronized(g) {
      g.setColor(Color.black);
      g.fillRect(x, y, width, height);
    }
  }

  public String toString() {
    return ("Paddle:: (" + x + "," + y + "):");
  }
}

