import java.awt.*;
import java.util.Vector;
import java.util.Date;
import java.util.Random;
import java.lang.Math;
import java.applet.AudioClip;


public class BricksApplet extends java.applet.Applet {
  BricksGame game;
  CommandPanel cp;
  Panel p1;
  private AudioClip efx[];

  public void init () {
    System.out.println("Initializing.");

    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();

    this.setLayout(gb);

    game = new BricksGame(this);
    gbc.fill = GridBagConstraints.BOTH;
    gbc.gridwidth = GridBagConstraints.REMAINDER;
    gbc.weightx = gbc.weighty = 1.0;
    gb.setConstraints(game, gbc);
    this.add(game);

    cp = new CommandPanel(this);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridwidth = GridBagConstraints.REMAINDER;
    gbc.weightx = gbc.weighty = 0.0;
    gb.setConstraints(cp, gbc);
    this.add(cp);

    efx = new AudioClip[BricksConstants.EFX_LENGTH];
    efx[BricksConstants.EFX_IMPACT_WALL] = 
      getAudioClip(getCodeBase(), "audio/impact-wall.au");
    efx[BricksConstants.EFX_IMPACT_PADDLE] = 
      getAudioClip(getCodeBase(), "audio/impact-paddle.au");
    efx[BricksConstants.EFX_IMPACT_BRICK] = 
      getAudioClip(getCodeBase(), "audio/impact-brick.au");
    efx[BricksConstants.EFX_IMPACT_BRICK_SPECIAL] = 
      getAudioClip(getCodeBase(), "audio/impact-brick-special.au");
    efx[BricksConstants.EFX_LOST_BALL] = 
      getAudioClip(getCodeBase(), "audio/lost-ball.au");
    efx[BricksConstants.EFX_NEXT_LEVEL] = 
      getAudioClip(getCodeBase(), "audio/next-level.au");

  }

  public final AudioClip[] getEfx() {
    return efx;
  }

  public void start () {
    System.out.println(this + "Start.");

    game.start();
    repaint();
  }

  public void destroy () {
    System.out.println(this + "Destroy.");
    game.destroy();
  }

  public void stop () {
    System.out.println(this + "Stop.");
    game.stop();
  }

  public void pause () {
    System.out.println(this + "Pause.");
    game.pause();
  }

  public void resume () {
    System.out.println(this + "Resume.");
    game.resume();
  }

  public void reset () {
    System.out.println(this + "Reset.");
    game.reset();
  }

  public Insets insets () {
    return new Insets(10,10,10,10);
  }


}

class CommandPanel extends Panel {
  private BricksApplet parent;
  private Button reset, pause;
  
  public CommandPanel(BricksApplet parent) {

    System.out.println("CommandPanel:Create.");

    this.parent = parent;

    this.add(new Label("Score: "));
    Label score = new Label("0000");

    this.add(score);

    reset = new Button("Reset");
    pause = new Button("Pause");

    this.add(reset);
    this.add(pause);
    this.layout();

    ScoreKeeper s = new ScoreKeeper(parent.game, (Panel) this, score);
    s.start();

  }

  public boolean action (Event evt, Object obj) {
    System.out.println("CommandPanel: got event.");

    if (evt.target instanceof Button) {
      return buttonAction(evt, obj);
    } else {
      return false;
    }

  }

  public boolean buttonAction (Event evt, Object obj) {
    System.out.println("CommandPanel: Handling button event.");

    String which;

    which = ((Button) evt.target).getLabel();
    System.out.println("CommandPanel: Handling '" + which + "'.");

    if (which.equals("Reset")) {
      pause.setLabel("Pause");
      parent.reset();
      return true;
    } else if (which.equals("Pause")) {
      pause.setLabel("Resume"); this.layout();
      parent.pause();
      return true;
    } else if (which.equals("Resume")) {
      pause.setLabel("Pause"); this.layout();
      parent.resume();
      return true;
    } else
      return false;

  }

}


class ScoreKeeper extends Thread {

  BricksGame game;
  Panel p;
  Label score;

  ScoreKeeper (BricksGame game, Panel p, Label l) {
    this.game = game;
    this.p = p;
    this.score = l;
  }

  public void run () {
    setPriority(2);

    while (true) {
      if (game != null && game.ball != null) {
        score.setText(Integer.toString(game.getBall().getScore()));
      }

      try {
        sleep(3600);
      } catch (InterruptedException e) {
        //
      }

    }

  }

}

  

