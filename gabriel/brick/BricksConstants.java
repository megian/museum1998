
public class BricksConstants {
  final static int IMPACT_NONE = 0;
  final static int IMPACT_LEFT = 1;
  final static int IMPACT_RIGHT = 2;
  final static int IMPACT_TOP = 3;
  final static int IMPACT_BOTTOM = 4;


  final static int EFX_LENGTH=6;
  final static int EFX_IMPACT_WALL=0;
  final static int EFX_IMPACT_PADDLE=1;
  final static int EFX_IMPACT_BRICK=2;
  final static int EFX_IMPACT_BRICK_SPECIAL=3;
  final static int EFX_LOST_BALL=4;
  final static int EFX_NEXT_LEVEL=5;
  

}

